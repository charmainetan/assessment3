'use strict';

// Loads path to access helper functions for working with files and directory paths
var path = require("path");
var express = require("express");

module.exports = function(app, database) {
    // Defines paths
    // __dirname is a global that holds the directory name of the current module
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

    // MIDDLEWARES --------------------------------------------------------------------------------------------------------
    // Serves files from public directory (in this case CLIENT_FOLDER).
    // __dirname is the absolute path of the application directory.
    // if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
    app.use(express.static(CLIENT_FOLDER));

    // ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
    // Defines endpoint exposed to client side for registration
    app.post("/api/employees", function (req, res, next) {
        // Information sent via an HTTP POST is found in req.body
        console.log('\nInformation submitted to server:')
        console.log(req.body);
        
        // Employee
        //     .create({
        //         emp_no: req.body.emp.empNo,
        //         birth_date: new Date(req.body.emp.birthday),
        //         first_name: req.body.emp.firstname,
        //         last_name: req.body.emp.lastname,
        //         gender: req.body.emp.gender,
        //         hire_date: new Date(req.body.emp.hiredate)
        //     })
        //     .then(function (employee) {
        //         res
        //             .status(200)
        //             .json(employee);
        //     })
        //     .catch(function (err) {
        //         console.log(err);
        //         res
        //             .status(501)
        //             .json(err);
        //     });
        console.log(req.body.emp.dept_no);
        console.log(req.body.emp.department);
        
        database.connection
            .transaction(function (t) {
                return database.Employee
                    .create(
                        {
                            emp_no: req.body.emp.empNo,
                            birth_date: new Date(req.body.emp.birthday),
                            first_name: req.body.emp.firstname,
                            last_name: req.body.emp.lastname,
                            gender: req.body.emp.gender,
                            hire_date: new Date(req.body.emp.hiredate)
                        }
                        , {transaction: t})
                    .then(function (employee) {
                        console.log("inner result " + JSON.stringify(employee))
                    
                        return database.Department.findOne({where: {dept_no: req.body.emp.department}}, {transaction: t})
                            .then(function(result){
                                console.log(result.dept_no);
                                console.log(result.dept_name);
                                return database.DeptEmp.create(
                                    {
                                        emp_no: req.body.emp.empNo,
                                        dept_no: result.dept_no
                                        , dept_name: result.dept_name
                                        , from_date: new Date(req.body.emp.hiredate)
                                        , to_date: new Date(req.body.emp.hiredate)
                                    }
                                    , {transaction: t});
                                });
                        });
                        
            })
            .then(function (employee) {
                res
                    .status(200)
                    .json(employee);
            })
            .catch(function (err) {
                console.log(err);
                res
                    .status(501)
                    .json(err);
            });
    });

    // Defines endpoint handler exposed to client side for retrieving employee information from database. Client side
    // sent data as part of the query string, we access query string paramters via the req.query property
    app.get("/api/employees", function (req, res) {
        database.Employee
        // findAll asks sequelize to retrieve multiple records (all records if where clause not used, i.e., no filtering)
            .findAll({
                where: {
                    // This where condition filters the findAll result so that it only includes employee names and
                    // employee numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                    // string, the following
                    $or: [
                        {first_name: {$like: "%" + req.query.searchString + "%"}},
                        {last_name: {$like: "%" + req.query.searchString + "%"}},
                        {emp_no: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }
                // We add a limit since employees table is big
                , limit: 100
            })
            .then(function (employees) {
                res
                    .status(200)
                    .json(employees);
            })
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    /* Defines endpoint handler exposed to client side for retrieving employee records that match query string passed.
    Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
    string, we access query string paramters via the req.query property
    */
    app.get("/api/employees/departments", function (req, res) {
        database.Employee
        // Use findAll to retrieve multiple records
            .findAll({
                // Use the where clause to filter final result; e.g., when you only want to retrieve employees that have
                // "s" in its name
                where: {
                    // $or operator tells sequelize to retrieve record that match any of the condition
                    $or: [
                        // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                        // % allows you to match any string of zero or more characters
                        {first_name: {$like: "%" + req.query.searchString + "%"}},
                        {last_name: {$like: "%" + req.query.searchString + "%"}},
                        {emp_no: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }
                , limit: 100
                // What Include attribute does: Join two or more tables. In this instance:
                // 1. For every Employee record that matches the where condition, the include attribute returns
                // ALL employees that have served as managers of said Employee
                // 2. model attribute specifies which model to join with primary model
                // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
                // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
                , include: [{
                    model: database.DeptEmp
                    , order: [["to_date", "DESC"]]
                    , limit: 1
                    // We include the Employee model to get the manager's name
                    , include: [database.Department]
                }]
            })
            // this .then() handles successful findAll operation
            // in this example, findAll() used the callback function to return employees
            // we named it employeess, but this object also contains info about the
            // latest department of that employee
            .then(function (employees) {
                res
                    .status(200)
                    .json(employees);
            })
            // this .catch() handles erroneous findAll operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    // -- Searches for specific employees by emp_no

    app.get("/api/employees/:emp_no", function (req, res) {
        console.log
        var where = {};
        if (req.params.emp_no) {
            where.emp_no = req.params.emp_no
        }

        console.log("where " + where);
        // We use findOne because we know (by looking at the database schema) that emp_no is the primary key and
        // is therefore unique. We cannot use findByNo because findByNo does not support eager loading
        database.Employee
            .findOne({
                where: where
                , include: [{
                    model: database.DeptEmp
                    , order: [["to_date", "DESC"]]
                    , limit: 1
                    // We include the Employee model to get the manager's name
                    , include: [database.Department]
                }]
            })

            .then(function (employee) {
                console.log("-- GET /api/employees/:emp_no findOne then() result \n " + JSON.stringify(employee));
                res.json(employee);
            })
            // this .catch() handles erroneous findAll operation
            .catch(function (err) {
                console.log("-- GET /api/employees/:emp_no findOne catch() \n ");

                res
                    .status(500)
                    .json({error: true});
            });
    });

    // -- Updates employees
    app.put('/api/employees/:emp_no', function (req, res) {
        var where = {};
        where.emp_no = req.params.emp_no;

        // Updates employee detail
        console.log("body " + JSON.stringify(req.body));
        database.Employee
            .update(
                {first_name: req.body.first_name}
                , {where: where}                            // search condition / criteria
            )
            .then(function (employee) {
                res
                    .status(200)
                    .json(employee);
            })
            .catch(function (err) {
                console.log(err);
                res
                    .status(500)
                    .json(err);
            });
    });

    // -- Searches for and deletes employee of a specific employee num
    app.delete("/api/employees/:emp_no", function (req, res) {
        var where = {};
        where.emp_no = req.params.emp_no;

        database.Employee
            .destroy({
                where: where
            })
            .then(function (result) {
                if (result == "1")
                    res.json({success: true});
                else
                    res.json({success: false});
            })
            .catch(function (err) {
                console.log("-- DELETE /api/employees/:emp_no catch(): \n" + JSON.stringify(err));
            });
    });

    // Defines endpoint exposed to client side for retrieving all employee information (STATIC)
    app.get("/api/static/employees", function (req, res) {
        var employees = [
            {
                empNo: 1001,
                empFirstName: 'Emily',
                empLastName: 'Smith',
                empPhoneNumber: '6516 2093'

            }
            , {
                empNo: 1002,
                empFirstName: 'Varsha',
                empLastName: 'Jansen',
                empPhoneNumber: '6516 2093'
            }
            ,
            {
                empNo: 1003,
                empFirstName: 'Julie',
                empLastName: 'Black',
                empPhoneNumber: '6516 2093'
            }
            , {
                empNo: 1004,
                empFirstName: 'Fara',
                empLastName: 'Johnson',
                empPhoneNumber: '6516 2093'
            }
            ,
            {
                empNo: 1005,
                empFirstName: 'Justin',
                empLastName: 'Zhang',
                empPhoneNumber: '6516 2093'
            }
            , {
                empNo: 1006,
                empFirstName: 'Kenneth',
                empLastName: 'Black',
                empPhoneNumber: '6516 2093'
            }

        ];
        // Return employees as a json object
        res
            .status(200)
            .json(employees);
    });

    app.get("/api/departments", function (req, res) {
        database.connection
        // Explanation of SQL statement
        // 1. SELECT - SELECT specifies that this is a read/retrieve command
        // 2. dept_no, ... - identifies the columns to return; use * to return all columns
        // 3. FROM departments - specifies the table to read data from
            .query("SELECT dept_no, dept_name " +
                "FROM departments "
            )
            // this .spread() handles successful native query operation
            // we use .spread instead of .then so as to separate metadata from the emplooyee records
            .spread(function (departments) {
                res
                    .status(200)
                    .json(departments);
            })
            // this .catch() handles erroneous native query operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    // Handles 404. In Express, 404 responses are not the result of an error,
    // so the error-handler middleware will not capture them.
    // To handle a 404 response, add a middleware function at the very bottom of the stack
    // (below all other path handlers)
    app.use(function (req, res) {
        res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
    });

    // Error handler: server error
    app.use(function (err, req, res, next) {
        res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
    });
}